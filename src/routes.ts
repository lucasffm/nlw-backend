import express from "express";

import PointsController from "./controllers/PointsController";
import ItemsController from "./controllers/ItemsController";

const routes = express.Router();

// Items
const itemsController = new ItemsController();
routes.get("/items", itemsController.index);

// Points
const pointsController = new PointsController();
routes.post("/points", pointsController.store);
routes.get("/points", pointsController.index);
routes.get("/points/:id", pointsController.show);

export default routes;
